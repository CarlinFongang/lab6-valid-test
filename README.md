# Gitlab-CI | Validation Test

<p align="center">
Please find the specifications by clicking
  <a href="https://github.com/eazytraining/gitlab-ci-training/tree/2ea9e36e03909e19ceb2fae2d9d950b3dc8546e8/TPs%20Heroku/TP6%20-%20Test%20de%20validation" alt="Crédit : eazytraining.fr" >
  </a>
</p>

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


<img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 

LinkedIn : https://www.linkedin.com/in/carlinfongang/


## Objectif
1. Créer des jobs pour tester le fonctionnement de l'application web en staging et en prod à l'aide du curl et sur la branch master
2. Realiser un job template qui va permettre d'effectuer un curl dépendament de la valeur de la variable $DOMAIN à définir
3. Inserer le template de l'étape 2 aux jobs de l'étape 1.

## Exécution de la pipeline intégrant les test en staging et en production
>![Alt text](img/image.png)

## Test staging validé
le curl "https://$DOMAIN" renvoi une bonne réponse
la variable `$DOMAIN` est surchargé ici par le domaine de l'environnement de staging
>![Alt text](img/image-1.png)

## Test en production validé
la variable `$DOMAIN` à été surchargé à cette étape par le domaine de l'environnement en production
>![Alt text](img/image-2.png)

## Validation du pipeline 
>![Alt text](img/image-3.png)